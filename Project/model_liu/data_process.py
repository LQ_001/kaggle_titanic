import pandas as pd

train_df=pd.read_csv('./Project/dataset/titanic/train.csv') #训练集
test_df=pd.read_csv('./Project/dataset/titanic/test.csv')  #测试集
predict_df=pd.read_csv('./Project/dataset/titanic/gender_submission.csv')  #测试集

print("\n\n--------------------------训练集 train 的内容：\n\n", train_df)
print("\n\n--------------------------测试集 test  的内容：\n\n", test_df)
print("\n\n--------------------------提交结果predict模板: \n\n", predict_df)

# -------------------------------------------------------------------------------------------------------------------------------train的处理
# -----------------------------------------------------------------------------------处理缺失值
# 填充 Age 缺失值，按 Pclass 的中位数年龄填充
train_df['Age'] = train_df.groupby('Pclass')['Age'].transform(lambda x: x.fillna(x.median()))

# 填充 Embarked 缺失值，使用出现频率最高的值
train_df['Embarked'].fillna(train_df['Embarked'].mode()[0], inplace=True)
 
# 删除 Cabin 列，因其缺失值过多
train_df.drop(columns=['Cabin'], inplace=True)

# -----------------------------------------------------------------------------------处理类别型特征
# 独热编码处理 Sex 和 Embarked 特征
train_df = pd.get_dummies(train_df, columns=['Sex', 'Embarked'], drop_first=True)
 
# Pclass 可以保留原数值，不需要处理

# -----------------------------------------------------------------------------------创建新特征维度
# 创建 FamilySize 特征
train_df['FamilySize'] = train_df['SibSp'] + train_df['Parch'] + 1
 
# 创建 IsAlone 特征
train_df['IsAlone'] = (train_df['FamilySize'] == 1).astype(int)
 
# 从姓名中提取称谓（Title），并简化为常见的类别
train_df['Title'] = train_df['Name'].str.extract(r' ([A-Za-z]+)\.', expand=False)
train_df['Title'] = train_df['Title'].replace(['Lady', 'Countess', 'Capt', 'Col', 'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')
train_df['Title'] = train_df['Title'].replace('Mlle', 'Miss')
train_df['Title'] = train_df['Title'].replace('Ms', 'Miss')
train_df['Title'] = train_df['Title'].replace('Mme', 'Mrs')
 
# 独热编码处理 Title 特征
train_df = pd.get_dummies(train_df, columns=['Title'], drop_first=True)

# -----------------------------------------------------------------------------------删除无关特征
train_df.drop(columns=['PassengerId', 'Name', 'Ticket'], inplace=True)

# ----------------------------------------------------------------------------------检查数据类型
# 确保都为数值型
print("train_df 变量类型:\n\n", train_df.dtypes)
print("\n\n", train_df)

train_df.to_csv("./Project/model_liu/data_processed/train.csv", index=False)


# -------------------------------------------------------------------------------------------------------------------------------test的处理
# -----------------------------------------------------------------------------------处理缺失值
# 填充 Age 缺失值，按 Pclass 的中位数年龄填充
test_df['Age'] = test_df.groupby('Pclass')['Age'].transform(lambda x: x.fillna(x.median()))

# 填充 Embarked 缺失值，使用出现频率最高的值
test_df['Embarked'].fillna(test_df['Embarked'].mode()[0], inplace=True)
 
# 删除 Cabin 列，因其缺失值过多
test_df.drop(columns=['Cabin'], inplace=True)

# -----------------------------------------------------------------------------------处理类别型特征
# 独热编码处理 Sex 和 Embarked 特征
test_df = pd.get_dummies(test_df, columns=['Sex', 'Embarked'], drop_first=True)
 
# Pclass 可以保留原数值，不需要处理

# -----------------------------------------------------------------------------------创建新特征维度
# 创建 FamilySize 特征
test_df['FamilySize'] = test_df['SibSp'] + test_df['Parch'] + 1
 
# 创建 IsAlone 特征
test_df['IsAlone'] = (test_df['FamilySize'] == 1).astype(int)
 
# 从姓名中提取称谓（Title），并简化为常见的类别
test_df['Title'] = test_df['Name'].str.extract(r' ([A-Za-z]+)\.', expand=False)
test_df['Title'] = test_df['Title'].replace(['Lady', 'Countess', 'Capt', 'Col', 'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')
test_df['Title'] = test_df['Title'].replace('Mlle', 'Miss')
test_df['Title'] = test_df['Title'].replace('Ms', 'Miss')
test_df['Title'] = test_df['Title'].replace('Mme', 'Mrs')
 
# 独热编码处理 Title 特征
test_df = pd.get_dummies(test_df, columns=['Title'], drop_first=True)

# -----------------------------------------------------------------------------------删除无关特征
test_df.drop(columns=['Name', 'Ticket'], inplace=True)

# ----------------------------------------------------------------------------------检查数据类型
# 确保都为数值型
print("test_df 变量类型:\n\n", test_df.dtypes)
# print("\n\n", test_df)

test_df.to_csv("./Project/model_liu/data_processed/test.csv", index=False)