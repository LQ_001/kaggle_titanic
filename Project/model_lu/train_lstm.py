#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：kaggle_titanic 
@File    ：train_lstm.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/22 9:04 
@Brief   ：
"""

import os
import numpy as np
import torch.nn as nn
import torch
from torch.utils.data import DataLoader, TensorDataset
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score
from lstm import LSTMModel

# 数据路径
src_dir = "./data_processed/train"
data_pth = os.path.join(src_dir, 'data.npy')
labels_pth = os.path.join(src_dir, 'labels.npy')

dst_dir = "./lstm"
model_save_dir = os.path.join(dst_dir, "model_weight")
record_dir = os.path.join(dst_dir, "record")

if not os.path.exists(model_save_dir):
    os.makedirs(model_save_dir)

if not os.path.exists(record_dir):
    os.makedirs(record_dir)

# 加载数据
data = np.load(data_pth)
labels = np.load(labels_pth)

# 训练
folds = 10
fold_best_test_acc = [0 for i in range(folds)]
skf = StratifiedKFold(n_splits=folds, shuffle=True, random_state=2321)
for fold, (train_index, test_index) in enumerate(skf.split(data, labels)):
    x_train, x_test = data[train_index], data[test_index]
    y_train, y_test = labels[train_index], labels[test_index]

    train_dataset = TensorDataset(torch.from_numpy(x_train), torch.from_numpy(y_train))
    test_dataset = TensorDataset(torch.from_numpy(x_test), torch.from_numpy(y_test))

    train_loader = DataLoader(dataset=train_dataset, shuffle=True, batch_size=1000)
    test_loader = DataLoader(dataset=test_dataset, batch_size=1000)

    # 创建模型
    model = LSTMModel(input_size=1, hidden_size=32, num_layers=2, output_size=1).cuda()
    criterion = nn.BCELoss()  # 二分类交叉熵损失
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    epochs = 1000
    for ep in range(epochs):
        # ------------------------------------------------------------------------------------begin 训练
        train_predictions = []
        train_labels = []
        model.train()
        for batch_data, batch_labels in train_loader:
            batch_data = batch_data.unsqueeze(-1).float().cuda()
            batch_labels = batch_labels.float().cuda()

            outputs = model(batch_data)
            probabilities = torch.sigmoid(outputs).squeeze()

            loss = criterion(probabilities, batch_labels)

            # 反向传播和优化
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # 预测
            train_predictions.extend((probabilities >= 0.5).int().cpu())
            train_labels.extend(batch_labels.int().cpu())
        train_accuracy = accuracy_score(train_labels, train_predictions)
        # ------------------------------------------------------------------------------------end 训练

        # -------------------------------------------------------------------------------------begin 测试
        test_predictions = []
        test_labels = []
        model.eval()
        with torch.no_grad():
            for batch_data, batch_labels in test_loader:
                batch_data = batch_data.unsqueeze(-1).float().cuda()

                outputs = model(batch_data)
                probabilities = torch.sigmoid(outputs).squeeze()

                test_predictions.extend((probabilities >= 0.5).int().cpu())
                test_labels.extend(batch_labels.int().cpu())

        test_accuracy = accuracy_score(test_labels, test_predictions)
        # -------------------------------------------------------------------------------------end 测试

        record_str = f"FOLD: {fold + 1} - EPOCH: {ep + 1}/{epochs} - train_acc: {train_accuracy:.5f} - test_acc: {test_accuracy:.5f}"

        if test_accuracy >= fold_best_test_acc[fold]:
            fold_best_test_acc[fold] = test_accuracy
            record_str += f" <<--Update_Test_ACC! {test_accuracy:.5f}"

            # 保存模型参数
            torch.save(model.state_dict(), os.path.join(model_save_dir, f"lstm_fold_{fold}.pt"))

        print(record_str)

print(fold_best_test_acc)
print("mean:", np.mean(fold_best_test_acc))

with open(os.path.join(record_dir, 'result.txt'), 'w') as file:
    file.write(str(fold_best_test_acc) + '\n')
    file.write(str(np.mean(fold_best_test_acc)))



