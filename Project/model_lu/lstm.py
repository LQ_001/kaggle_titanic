#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：kaggle_titanic 
@File    ：lstm.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/22 9:03 
@Brief   ：
"""

import torch
import torch.nn as nn


class LSTMModel(nn.Module):
    def __init__(self, input_size=1, hidden_size=32, num_layers=2, output_size=1):
        super(LSTMModel, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers

        # LSTM层
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True)

        # 线性层
        self.fc = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        # 初始化隐藏状态和细胞状态
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).to(x.device)
        c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).to(x.device)

        # 前向传播LSTM
        out, _ = self.lstm(x, (h0, c0))

        # 取最后一个时间步的输出
        out = out[:, -1, :]

        # 通过线性层
        out = self.fc(out)
        return out


if __name__ == '__main__':
    # 假设输入数据x的形状为(batch_size, seq_length, input_size)
    # 在这个例子中，由于我们实际上并没有时间序列数据，我们需要将x的形状从(x, 7)调整为(batch_size, 7, 1)
    # 例如，如果我们有100个样本，我们可以这样做：
    batch_size = 100
    x = torch.randn(batch_size, 7)  # 假设的随机数据
    x = x.unsqueeze(-1)  # 将x的形状从(batch_size, seq_length)调整为(batch_size, seq_length, 1)

    # 创建模型实例
    model = LSTMModel(input_size=1, hidden_size=32, num_layers=2,
                      output_size=1)  # 输出大小取决于你的任务（例如，二分类可以为1，回归可以为1，多分类可以为类别数）

    # 选择损失函数和优化器（这里以二分类为例，使用BCELoss和Adam优化器）
    criterion = nn.BCELoss()  # 二分类交叉熵损失
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    # 假设y是形状为(batch_size,)的目标标签（对于二分类，标签应该是0或1的浮点数或经过sigmoid激活的输出）
    y = torch.randint(0, 2, (batch_size,)).float()  # 随机生成的目标标签（仅为示例）

    # 前向传播
    outputs = model(x)
    loss = criterion(torch.sigmoid(outputs).squeeze(), y)  # 使用sigmoid将输出转换为概率，并计算损失

    # 反向传播和优化
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    print(loss.item())  # 打印损失值
