import pandas as pd

train_df=pd.read_csv('./Project/model_liu/data_processed/train.csv') #训练集
test_df=pd.read_csv('./Project/model_liu/data_processed/test.csv')  #测试集

# -----------------------------------------------------------------------------------数据集拆分
from sklearn.model_selection import train_test_split
 
# 特征和目标变量
X = train_df.drop('Survived', axis=1)  # 特征
y = train_df['Survived']  # 目标变量
 
# 数据集拆分
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.01, random_state=42)

# -----------------------------------------------------------------------------------LightGBM模型
from lightgbm import LGBMClassifier  # pip install lightgbm -i https://pypi.tuna.tsinghua.edu.cn/simple
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
 

# 初始化并训练模型
lgbm = LGBMClassifier(random_state=42)
lgbm.fit(X_train, y_train)
 
# 预测
y_pred = lgbm.predict(X_test)
y_prob = lgbm.predict_proba(X_test)[:, 1]
 
# 模型评估
accuracy = accuracy_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
roc_auc = roc_auc_score(y_test, y_prob)
 
print(f"LightGBM模型 - 准确率: {accuracy:.4f}, F1分数: {f1:.4f}, AUC-ROC: {roc_auc:.4f}")



# -----------------------------------------------------------------------------------测试集预测
# 确保测试集的特征与训练集一致（如果有必要，进行列名排序或补齐缺失列）

passenger_ids = test_df['PassengerId'] 
test_df.drop(columns=['PassengerId'], inplace=True)  # 删除 PassengerId 列
test_predictions = lgbm.predict(test_df)  # 预测分类结果


# 将预测结果与 PassengerId 对应起来
results_df = pd.DataFrame({
    'PassengerId': passenger_ids,
    'Survived': test_predictions
})

# 保存结果到CSV文件
results_df.to_csv('./Project/model_liu/data_processed/predictions.csv', index=False)

print("测试集预测完成，结果已保存至 'predictions.csv'")
