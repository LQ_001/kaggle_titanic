#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：kaggle_titanic 
@File    ：data_process.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/21 19:45 
@Brief   ：
"""

import os.path
import numpy as np
import pandas as pd

# 加载路径
src_dir = "../dataset/titanic"
train_data_pth = os.path.join(src_dir, 'train.csv')

# 保存路径
dst_dir = "./data_processed/train"
os.makedirs(dst_dir, exist_ok=True)
data_pth = os.path.join(dst_dir, 'data.npy')
labels_pth = os.path.join(dst_dir, 'labels.npy')

# 加载数据
train_data_df = pd.read_csv(train_data_pth)
# print(train_data_df)

# 数据处理
Survived = train_data_df['Survived']        # 生存情况: 0->死亡；1->存活
Pclass = train_data_df['Pclass']            # 客场等级: 1, 2, 3
# Name = train_data_df['Name']                # 乘客姓名: string
Sex = train_data_df['Sex']                  # 乘客性别: male/female
Age = train_data_df['Age']                  # 乘客年龄: int or Nan
SibSp = train_data_df['SibSp']              # 在船兄妹姐妹数/配偶数: int
Parch = train_data_df['Parch']              # 在船父母数/子女数: int
# Ticket = train_data_df['Ticket']            # 船票编号: string
Fare = train_data_df['Fare']                # 船票价格: float
# Cabin = train_data_df['Cabin']              # 客舱号: string or Nan
Embarked = train_data_df['Embarked']        # 登船港口: S, C, Q or Nan

# 样本数据
data = []
# Pclass
data.append(Pclass.to_numpy(dtype=np.float32))

# Sex
# 将male/female转换为0/1
Sex = Sex.replace({'male': 0, 'female': 1})
data.append(Sex.to_numpy(dtype=np.float32))

# Age
Age = Age.fillna(-1)
data.append(Age.to_numpy(dtype=np.float32))

# SibSp
data.append(SibSp.to_numpy(dtype=np.float32))

# Parch
data.append(Parch.to_numpy(dtype=np.float32))

# Fare
data.append(Fare.to_numpy(dtype=np.float32))

# Embarked
Embarked = Embarked.replace({'S': 0, 'C': 1, 'Q': 2})
Embarked = Embarked.fillna(-1)
data.append(Embarked.to_numpy(dtype=np.float32))

data = np.stack(data, axis=1)
print(data.shape)
np.save(data_pth, data)

# 标签
labels = Survived.to_numpy(dtype=np.int32)
np.save(labels_pth, labels)







