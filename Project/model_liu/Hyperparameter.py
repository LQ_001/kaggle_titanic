import pandas as pd

train_df=pd.read_csv('./Project/model_liu/data_processed/train.csv') #训练集
test_df=pd.read_csv('./Project/model_liu/data_processed/test.csv')  #测试集

# -----------------------------------------------------------------------------------数据集拆分
from sklearn.model_selection import train_test_split
 
# 特征和目标变量
X = train_df.drop('Survived', axis=1)  # 特征
y = train_df['Survived']  # 目标变量
 
# 数据集拆分
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# -----------------------------------------------------------------------------------超参数调优模型
from lightgbm import LGBMClassifier  # pip install lightgbm -i https://pypi.tuna.tsinghua.edu.cn/simple
from xgboost import XGBClassifier  # pip install xgboost -i https://pypi.tuna.tsinghua.edu.cn/simple
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
from sklearn.model_selection import GridSearchCV
 
# 随机森林超参数调优
param_grid_rf = {
    'n_estimators': [100, 200, 300],
    'max_depth': [10, 20, 30],
    'min_samples_split': [2, 5, 10]
}
grid_search_rf = GridSearchCV(estimator=RandomForestClassifier(random_state=42), param_grid=param_grid_rf, cv=5, verbose=3, scoring='roc_auc', n_jobs=-1)
grid_search_rf.fit(X_train, y_train)
best_rf = grid_search_rf.best_estimator_

# 预测
y_pred = best_rf.predict(X_test)
y_prob = best_rf.predict_proba(X_test)[:, 1]
 
# 模型评估
accuracy = accuracy_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
roc_auc = roc_auc_score(y_test, y_prob)
print(f"随机森林模型（超参数调优） - 准确率: {accuracy:.4f}, F1分数: {f1:.4f}, AUC-ROC: {roc_auc:.4f}")
 

# XGBoost超参数调优
param_grid_xgb = {
    'n_estimators': [100, 200, 300],
    'max_depth': [3, 5, 7],
    'learning_rate': [0.01, 0.1, 0.2],
    'subsample': [0.8, 0.9, 1.0]
}
grid_search_xgb = GridSearchCV(estimator=XGBClassifier(eval_metric='logloss', random_state=42), param_grid=param_grid_xgb, cv=5, verbose=3, scoring='roc_auc', n_jobs=-1)
grid_search_xgb.fit(X_train, y_train)
best_xgb = grid_search_xgb.best_estimator_

# 预测
y_pred = best_xgb.predict(X_test)
y_prob = best_xgb.predict_proba(X_test)[:, 1]
 
# 模型评估
accuracy = accuracy_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
roc_auc = roc_auc_score(y_test, y_prob)
print(f"XGBoost模型（超参数调优） - 准确率: {accuracy:.4f}, F1分数: {f1:.4f}, AUC-ROC: {roc_auc:.4f}")

# LightGBM超参数调优
param_grid_lgbm = {
    'n_estimators': [100, 200, 300],
    'max_depth': [3, 5, 7],
    'learning_rate': [0.01, 0.1, 0.2],
    'num_leaves': [31, 63, 127]
}
grid_search_lgbm = GridSearchCV(estimator=LGBMClassifier(random_state=42), param_grid=param_grid_lgbm, cv=5, verbose=3, scoring='roc_auc', n_jobs=-1)
grid_search_lgbm.fit(X_train, y_train)
best_lgbm = grid_search_lgbm.best_estimator_

# 预测
y_pred = best_lgbm.predict(X_test)
y_prob = best_lgbm.predict_proba(X_test)[:, 1]
 
# 模型评估
accuracy = accuracy_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
roc_auc = roc_auc_score(y_test, y_prob)
print(f"LightGBM模型（超参数调优） - 准确率: {accuracy:.4f}, F1分数: {f1:.4f}, AUC-ROC: {roc_auc:.4f}")