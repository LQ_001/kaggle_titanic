import pandas as pd

train_df=pd.read_csv('./Project/model_liu/data_processed/train.csv') #训练集
test_df=pd.read_csv('./Project/model_liu/data_processed/test.csv')  #测试集

# -----------------------------------------------------------------------------------数据集拆分
from sklearn.model_selection import train_test_split
 
# 特征和目标变量
X = train_df.drop('Survived', axis=1)  # 特征
y = train_df['Survived']  # 目标变量
 
# 数据集拆分
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# -----------------------------------------------------------------------------------逻辑回归模型
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
 
# 初始化并训练模型
logreg = LogisticRegression(max_iter=5000)
logreg.fit(X_train, y_train)
 
# 预测
y_pred = logreg.predict(X_test)
y_prob = logreg.predict_proba(X_test)[:, 1]
 
# 模型评估
accuracy = accuracy_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
roc_auc = roc_auc_score(y_test, y_prob)
 
print(f"逻辑回归模型 - 准确率: {accuracy:.4f}, F1分数: {f1:.4f}, AUC-ROC: {roc_auc:.4f}")
