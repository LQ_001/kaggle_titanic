#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：kaggle_titanic 
@File    ：svm.py
@IDE     ：PyCharm 
@Author  ：maxluuu@126.com
@Date    ：2024/11/21 20:13 
@Brief   ：
"""

import os.path

import numpy as np
from sklearn import svm
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score


# 数据路径
src_dir = "./data_processed/train"
data_pth = os.path.join(src_dir, 'data.npy')
labels_pth = os.path.join(src_dir, 'labels.npy')

# 加载数据
data = np.load(data_pth)
labels = np.load(labels_pth)

# 训练
accuracies = []
skf = StratifiedKFold(n_splits=5, shuffle=True, random_state=42)
for train_index, test_index in skf.split(data, labels):
    X_train, X_test = data[train_index], data[test_index]
    y_train, y_test = labels[train_index], labels[test_index]

    # 创建一个 SVM 分类器
    clf = svm.SVC(kernel='linear')

    # 使用训练集训练分类器
    clf.fit(X_train, y_train)

    # 使用测试集进行预测
    y_pred = clf.predict(X_test)

    # 计算准确率并添加到列表中
    accuracy = accuracy_score(y_test, y_pred)
    accuracies.append(accuracy)

# 打印每折的准确率和平均准确率
for i, acc in enumerate(accuracies):
    print(f"Fold {i+1} Accuracy: {acc:.2f}")
print(f"Mean Accuracy: {np.mean(accuracies):.2f}")